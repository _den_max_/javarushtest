package com.vk.idmakdenisov;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Класс запуска приложения
 * Created by max on 27.06.17.
 */
@SpringBootApplication
@ComponentScan("com.vk.idmakdenisov")
@org.springframework.context.annotation.Configuration
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

   @Bean(name = "SessionFactory")
    public SessionFactory getSessionFactory() {
        Configuration cfg=new Configuration();
        cfg.configure("hibernate.cfg.xml");
        return  cfg.buildSessionFactory();
   }

}
