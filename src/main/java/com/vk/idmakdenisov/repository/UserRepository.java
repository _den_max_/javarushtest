package com.vk.idmakdenisov.repository;

import com.vk.idmakdenisov.domain.UserEntity;

import java.util.List;

/**
 * Интерфейс CRUD
 * Created by max on 22.06.17.
 */
public interface UserRepository {
    void deleteUser(int user);

    void addUser(UserEntity user);

    void updateUser(UserEntity user);

    UserEntity getUserById(int id);

    List getPageUsersByName(int page, String name);
}
