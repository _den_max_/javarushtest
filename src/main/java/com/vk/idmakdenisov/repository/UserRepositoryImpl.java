package com.vk.idmakdenisov.repository;

import com.vk.idmakdenisov.domain.UserEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final int PAGE_SIZE = 10;

    private final SessionFactory factory ;

    @Autowired
    public UserRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    private Session getSession() {
        Session session = factory.getCurrentSession();
        if (session == null) {
            session = factory.openSession();
        }
        return session;
    }

    @Override
    public void deleteUser(int user) {
        try {
            getSession().beginTransaction();
            UserEntity entity = getSession().load(UserEntity.class, user);
            getSession().delete(entity);
            getSession().getTransaction().commit();
        }
        catch(HibernateException e){
            getSession().getTransaction().rollback();
        }
    }

    @Override
    public void addUser(UserEntity user) {
        try{
            getSession().beginTransaction();
            getSession().save(user);
            getSession().getTransaction().commit();
        }
        catch(HibernateException e){
            getSession().getTransaction().rollback();
        }
    }

    @Override
    @Deprecated
    public void updateUser(UserEntity user) {
        try{
            getSession().beginTransaction();
            getSession().update(user);
            getSession().getTransaction().commit();
        }
        catch(HibernateException e){
            getSession().getTransaction().rollback();
        }
    }

    @Override
    public UserEntity getUserById(int id) {
        UserEntity userEntity = null;
        try{
            getSession().beginTransaction();
            userEntity = getSession().get(UserEntity.class, id);
            getSession().getTransaction().commit();
        }
        catch(HibernateException e){
            getSession().getTransaction().rollback();
        }
        return userEntity;
    }

    @Override
    public List getPageUsersByName(int page, String name) {
        List users = null;
        name = String.format("%s%%",name);
        try {
            getSession().beginTransaction();
            Query query =
                    getSession().createQuery("FROM UserEntity WHERE name like '"+ name +"' order by id");
            query.setFirstResult(page * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);
            users = query.list();
            getSession().getTransaction().commit();
        }
        catch(HibernateException e){
            getSession().getTransaction().rollback();
        }
        //noinspection unchecked
        return (List<UserEntity>)users;
    }
}
