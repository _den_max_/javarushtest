package com.vk.idmakdenisov.service;

import com.vk.idmakdenisov.domain.UserEntity;

import java.util.List;

/**
 * Created by max on 28.06.17.
 */
public interface UserService {
    void add(UserEntity userEntity);

    void delete(int userEntity);

    void update(UserEntity userEntity);

    UserEntity getUserById(int id);

    List<UserEntity> getPageUsersByName(int name, String s);
}
