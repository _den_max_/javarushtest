package com.vk.idmakdenisov.service;

import com.vk.idmakdenisov.domain.UserEntity;
import com.vk.idmakdenisov.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by max on 28.06.17.
 */
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;

    }

    @Override
    public void add(UserEntity userEntity) {
        repository.addUser(userEntity);
    }

    @Override
    public void delete(int userEntity) {
        repository.deleteUser(userEntity);
    }

    @Override
    public void update(UserEntity userEntity) {
            repository.updateUser(userEntity);
    }

    @Override
    public UserEntity getUserById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public List<UserEntity> getPageUsersByName(int page, String name) {
        return repository.getPageUsersByName( page, name);
    }


}
