package com.vk.idmakdenisov.web;

import com.vk.idmakdenisov.domain.UserEntity;
import com.vk.idmakdenisov.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by max on 27.06.17.
 */
@Controller
public class MainController {
    static int count = 0;
    private final UserService service ;

    @Autowired
    public MainController(UserService service) {
        this.service = service;
    }

    @RequestMapping(value="/",method = RequestMethod.GET)
    public String index() {
        return "table.html";
    }

    @RequestMapping(
            value="/delete",
            params = {"id", "page","name" },
            method = RequestMethod.GET)
    public @ResponseBody
    List<UserEntity> deleteUserById(
            @RequestParam("page") int page,
            @RequestParam("id") int id,
            @RequestParam("name") String name) {
        service.delete(id);
        return service.getPageUsersByName(page,name);
    }

    @RequestMapping(
            value = "/users",
            params = { "page", "name" },
            method = RequestMethod.GET)
    @ResponseBody
    public List<UserEntity> getUsersPageByName(@RequestParam("page") long page, @RequestParam("name") String name) {
         return service.getPageUsersByName((int) page,name);
    }

    @RequestMapping(
            value = "/add",
            method = RequestMethod.POST)
    @ResponseBody
    public List<UserEntity> addUser(@RequestBody UserEntity userEntity) {
        userEntity.setCreateDate(new Timestamp(System.currentTimeMillis()));
        service.add(userEntity);
        return service.getPageUsersByName(0,userEntity.getName());
    }

    @RequestMapping(
            value = "/update",
            method = RequestMethod.POST)
    @ResponseBody
    public List<UserEntity> updateUser(@RequestBody UserEntity userEntity) {
        userEntity.setCreateDate(new Timestamp(System.currentTimeMillis()));
        service.update(userEntity);
        return service.getPageUsersByName(0,userEntity.getName());
    }
}
