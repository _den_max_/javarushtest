/**
 * Created by max on 28.06.17.
 */
(function () {
    angular.module('UsersApp', [])
        .controller('UserController', function ($scope, $http) {
            var page = 0;
            $scope.name = "";
            $scope.updateUsers = function () {
                $http.get('/users', {
                    params: {
                        page: page,
                        name:  $scope.name
                    }
                })
                    .success(function (data, status, headers, config) {
                        if(data.length!=0){
                            $scope.users = data;
                        }
                        else{
                            page = page - 1;
                        }
                    });
            };
            $scope.updateUsers();

            $scope.loadNextPage = function () {
                if(page<0)
                    page=0;//хз
                page = page + 1;
                $scope.updateUsers()
            };

            $scope.loadPreviousPage = function () {
                if (page > 0) {
                    page = page - 1;
                }
                $scope.updateUsers();
            };

            $scope.deleteUser = function (id) {
                $http.get('/delete', {
                    params: {
                        page: page,
                        id: id,
                        name:$scope.name
                    }
                }).success(function (data, status, headers, config) {
                      $scope.users = data;
                });
            };

            $scope.addUser = function () {
                var dataObj = {
                    name : $scope.add_name,
                    age  : $scope.add_age,
                    admin: $scope.add_admin
                };
                $http.post('/add',dataObj).success(function (data, status, headers, config) {
                    $scope.users = data;
                    $scope.name = $scope.add_name;
                    document.getElementById("form_add").hidden = true;
                });
            };


            $scope.showAddUser = function () {
                $scope.add_admin = false;
                $scope.add_name = false;
                $scope.add_age = false;
                document.getElementById("form_add").hidden = false;
            };

            $scope.showUpdateUser = function (user) {
                $scope.update_admin = user.admin;
                $scope.update_name = user.name;
                $scope.update_age = user.age;
                $scope.update_id = user.id;
                document.getElementById("form_update").hidden = false;
            };

            $scope.updateUser = function () {
                var dataObj = {
                    id : $scope.update_id,
                    name : $scope.update_name,
                    age  : $scope.update_age,
                    admin: $scope.update_admin
                };
                $http.post('/update',dataObj).success(function (data, status, headers, config) {
                    $scope.users = data;
                    $scope.name = $scope.update_name;
                    document.getElementById("form_update").hidden = true;
                });
            };

        });
})();
