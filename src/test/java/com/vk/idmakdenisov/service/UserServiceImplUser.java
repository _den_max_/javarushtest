package com.vk.idmakdenisov.service;

import com.vk.idmakdenisov.domain.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Тестирование сервиса
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserServiceImplUser {

    @Autowired
    UserService service;

    @Test
    public void addUser() throws Exception{
        UserEntity userEntity = new UserEntity();
        userEntity.setAdmin(true);
        userEntity.setAge(100);
        userEntity.setCreateDate(new Timestamp(System.currentTimeMillis()));
        userEntity.setName("Batman");
        service.add(userEntity);
        List<UserEntity> list = service.getPageUsersByName(0, "Batman");
        assertTrue(!list.isEmpty());
    }


    @Test
    public void deleteUser() throws Exception{
        List<UserEntity> list = service.getPageUsersByName(0, "");
        service.delete(list.get(0).getId());
        List<UserEntity> list1 = service.getPageUsersByName(0,list.get(0).getName());
        assertTrue(!list1.contains(list.get(0)));
    }

    @Test
    public void getPageUsersByName() throws Exception {
        List<UserEntity> list = service.getPageUsersByName(0,"");
        assertTrue(!list.isEmpty());
    }

    @Test
    public void updateUsersByName() throws Exception {
        List<UserEntity> list = service.getPageUsersByName(0,"");
        UserEntity entity = list.get(0);
        entity.setAge(entity.getAge()+1);
        service.update(entity);
        UserEntity result= service.getUserById(entity.getId());
        assertTrue(result.equals(entity));
    }
}